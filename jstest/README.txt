1) Start Server
$ python3 manage.py runserver

2) Browse either one from the following:

// Main Sample
http://localhost:8000/charts

// Check for URL dispatcher
http://localhost:8000/charts/api/data
http://localhost:8000/charts/api/data/chart
