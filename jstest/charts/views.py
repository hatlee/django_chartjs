from django.shortcuts import render
from django.views.generic import View
from django.http import JsonResponse, HttpResponse


class HomeView(View):

    # Default page http://localhost:8000/charts
    def get(self, request, *args, **kwargs):
        data = {
            "app": "Django Sample",
            "num": range(10)
        }
        return render(request, "charts.html", data)


    def __str__(self):
        return "HomeView"


# API in views
def get_data(request, *args, **kwargs):
    data = {
        "LotID": "TOOLID-2-20180115",
        "WaferNum": 3,
        "WaferData": [
            {
                "X": {
                    "value": 100,
                    "color": "#F7464A",
                    "highlight": "#FF5A5E",
                    "label": "Red"
                },
                "Y": {
                    "value": 200,
                    "color": "#46BFBD",
                    "highlight": "#5AD3D1",
                    "label": "Green"
                },
                "Z": {
                    "value": 300,
                    "color": "#FDB45C",
                    "highlight": "#FFC870",
                    "label": "Yellow"
                }
            },
            {
                "X": {
                    "value": 300,
                    "color": "#F7464A",
                    "highlight": "#FF5A5E",
                    "label": "Red"
                },
                "Y": {
                    "value": 200,
                    "color": "#46BFBD",
                    "highlight": "#5AD3D1",
                    "label": "Green"
                },
                "Z": {
                    "value": 100,
                    "color": "#FDB45C",
                    "highlight": "#FFC870",
                    "label": "Yellow"
                }
            },
            {
                "X": {
                    "value": 300,
                    "color": "#F7464A",
                    "highlight": "#FF5A5E",
                    "label": "Red"
                },
                "Y": {
                    "value": 100,
                    "color": "#46BFBD",
                    "highlight": "#5AD3D1",
                    "label": "Green"
                },
                "Z": {
                    "value": 300,
                    "color": "#FDB45C",
                    "highlight": "#FFC870",
                    "label": "Yellow"
                }
            }
        ]
    }

    return JsonResponse(data)
