from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.HomeView.as_view(), name='charts-index'),
    path('api/data/', views.get_data, name='get-data'),
    path('api/data/chart/', views.get_data, name='get-chart'),
]